#!/usr/bin/python
import sys,datetime,argparse,psycopg2,os,ConfigParser,subprocess
version="1.0.0"
#Command Line Argument parser and help display
parser = argparse.ArgumentParser(description='Automated Backup and Recovery using pg_basebackup or pgbarman or pgbackrest',
	epilog='Example 1: Validate if Configs are set properly. Displays the Backup Type Selected. \n %(prog)s --precheck \n'
    'Example 2: Take a Backup using the Backup Type Selected in the configfile\n %(prog)s --backup --logile=<location>/logfilename>',
	formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--precheck', action='store_true',help="Validate the existing Configuration and show me the details")
parser.add_argument('--connection',help="Connection string containing host, username, password etc")
parser.add_argument('--logfile',help="Generate a logfile to the specified logfile with location")
parser.add_argument('--configfile',help="Config File being used by Script")
parser.add_argument('--kill',help="Kill running Backup/s that was started by Me!")
parser.add_argument('--listpids',action='store_true',help="List the pid's of Currently Running Backups")
parser.add_argument('--listbackups',action='store_true',help="List all the Succeeded Backups")
parser.add_argument('--execute', action='store_true',help="Execute the generated DDLs against database")
parser.add_argument('--version',action='store_true',help="List the VERSION of the Script")
parser.add_argument('--backup', action='store_true',help="Run the Backup using the Backup Type selected in the configuration file")
if len(sys.argv)==1:
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()

#Print the version of this program to stdout
def print_version():
    print("Version: "+version)
#
def read_backrestconf():
	print "None"	
def precheck():
	config = ConfigParser.RawConfigParser()
        config.read("bkup.conf")
	try:
		global pgbackrest
		pgbackrest=int(config.get('backuptype', 'pgbackrest'))
	except:
		pgbackrest = 0
	try:
		global pgbarman
		pgbarman=int(config.get("backuptype", "pgbarman"))
	except:
		pgbarman = 0
	try:
		global pg_basebackup
		pg_basebackup=int(config.get("backuptype","pg_basebackup"))
	except:
		pg_basebackup=0
	global backup_type
	if pgbackrest == 1:
		backup_type = 1
		print "Backup Type Selected is pgbackrest"
	elif pgbarman == 1:
		backup_type = 2
		print "Backup Type Selected is pgbarman"
	elif pg_basebackup == 1:
		backup_type = 3
		print "Backup Type Selected is pg_basebackup"
	else: 
		global stop_backup
		stop_backup = 1
		backup_type = 0
		print """################# --------------------  ################# \n No Backups have been enabled in the Config File. \n """
		print """To Enable Configs, set the value of the type of backup you need to 1. \n ################# --------------------  #################"""
#
def run_backrest():
#Create and Open and Output File for Writing pgbackrest logs and error logs
	out=open(backuplog, "w")
#	err=open(backuplog, "w")	
#	p = subprocess.Popen(["/bin/pgbackrest", "--stanza=demo", "--log-level-console=info", "--type=full", "backup"],stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	p = subprocess.Popen(["/bin/pgbackrest", "--stanza=demo", "--log-level-console=info", "--type=full", "backup"],stdin=out, stdout=out, stderr=out)
	print "PID:", p.pid		
def run_backup():
	precheck()
	if pgbackrest == 1:
        	print "Running Backup using pgbackrest"
		run_backrest()
	if pgbarman == 1:
		print "Running Backup using pgbarman"
	if pg_basebackup == 1:
		print "Running Backup using pg_basebackup"
		run_basebackup()
global backuplog
#
def list_backup():
	precheck()	
	if backup_type == 1:
		p = subprocess.Popen(["/bin/pgbackrest", "--stanza=demo", "info"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		print p.stdout.read()
	elif backup_type == 2:
		print "Lists All the PgBarman Backup Logs"
	elif backup_type == 3:
		print "Lists all the pg_basebackup Logs"
def get_date():
	todaysdate = []
	today = datetime.date.today()
	todaysdate.append(today)
	global backup_date
	backup_date=todaysdate[0]
	print "Backup Date is : " + str(backup_date)  

def send_err_mail():
	print "None"

def send_success_mail():
	print "Success"
def run_basebackup():
        config = ConfigParser.RawConfigParser()
        config.read("bkup.conf")
        try:
                basebackup_location=config.get('basebackup', 'location')
#		print "Basebackup Location is " + basebackup_location
		get_date()
		basebackup_location_final=(basebackup_location+"/"+str(backup_date))
		print "Backup location is : "+basebackup_location_final
	except:
                print "Backup Failed as no Backup Location in the Config File"
		send_err_mail()
		exit()	
        try:
                basebackup_host=config.get('basebackup', 'hostname')
#                print "Basebackup Host is " + basebackup_host
        except:
                print "Backup Failed as no Username in the conf file"
                send_err_mail()
                exit()
        try:
                basebackup_user=config.get('basebackup', 'username')
#               print "Basebackup User is " + basebackup_user
        except:
                print "Backup Failed as no Basebackup User in the conf file"
                send_err_mail()
                exit()
        try:
                basebackup_port=config.get('basebackup', 'port')
#                print "Basebackup Port is " + basebackup_port
        except:
                print "Backup Failed as no Basebackup Port in the conf file"
                send_err_mail()
                exit()
        try:
                basebackup_bin=config.get('basebackup', 'path_for_bin')
#               print "Basebackup Bin is " + basebackup_bin
		basebackup_with_path=(basebackup_bin+"/pg_basebackup")
        except:
                print "Backup Failed as no Basebackup Bin in the conf file"
                send_err_mail()
                exit()
        try:
#		out=open(backuplog, "w")
		log_filenew=open("/tmp/basebackuperror.log", "w")
        	p = subprocess.Popen([basebackup_with_path, "-h"+basebackup_host, "-D"+basebackup_location_final, "-p"+basebackup_port, "-U"+basebackup_user, "-Ft", "-x", "-l","backup_label"],stdin=log_file, stdout=log_file, stderr=log_filenew)
        	print "PID:", p.pid
		num_errors = sum(1 for line in open('/tmp/basebackuperror.log'))
		if num_errors > 0:			
#		if os.path.isfile("/tmp/basebackuperror.log"):
			print "Basebackup Failed with the following errors : "
			err = open('/tmp/basebackuperror.log', 'r')
			print err.read()
			err.close()
			send_err_mail()
		else:
			print "Basebackup Succeeded !!!"
			send_success_mail()
	except:
		print "Basebackup failed, please look into the log : " + backuplog
                send_err_mail()
                exit()

#main() function of the program
#if __name__ == "__main__":
    #print_version()
if args.version:
	print_version()
#elif args.list:
#	list_backrest_backups()
elif args.precheck:
	precheck()
elif args.backup and args.logfile:
	backuplog = args.logfile
	old_stdout = sys.stdout
	global log_file
	log_file = open(backuplog,"w")
	sys.stdout = log_file
#	precheck()
	run_backup()
	sys.stdout = old_stdout
	log_file.close()
elif args.backup:
	print "No Logfile mentioned. Hence, i consider taking the Backup to the default"
	backuplog = "/tmp/out.log"
	precheck()
	run_backup()
elif args.listbackups:
	print "Here are the List of all the Backups"
	list_backup()

#print "Backup Type Variable is :"+str(backup_type)
## Log Output is currently hard-coded default to /tmp.out.log when no logfile specified...
## 

